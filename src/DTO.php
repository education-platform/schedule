<?php

namespace Schedule;

class DTO
{
    public $disciplines;

    public $schedule;

    public function __construct($disciplines, Schedule $schedule)
    {
        $this->disciplines = $disciplines;
        $this->schedule = $schedule;
    }

    public function getDiscipline()
    {
        return $this->disciplines;
    }

    public function getSchedule()
    {
        return $this->schedule;
    }
}