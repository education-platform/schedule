<?php

namespace Schedule;

use Schedule\Console\ScheduleTable;

class Schedule
{
    use ConsoleHelper;

    public $days;

    public function addDay(Day $day)
    {
        $this->days[] = $day;
    }

    public function getDays()
    {
        return $this->days;
    }

    public function print()
    {
        (new ScheduleTable($this))->print();
    }
}