<?php

namespace Schedule;

use Schedule\Console\ScheduleTable;

class Lifecycle
{
    use ConsoleHelper;

    /**
     * @var DTO
     */
    private $dto;

    public function __construct()
    {
        $this->up();
        $this->build();
        $this->analyse();
        $this->down();
    }

    public function up()
    {
        $schedule = new Schedule();
        $schedule->addDay(new Day('Monday', 6));
        $schedule->addDay(new Day('Thursday', 6));
        $schedule->addDay(new Day('Wednesday', 6));
        $schedule->addDay(new Day('Thursday', 6));
        $schedule->addDay(new Day('Friday', 6));
        $schedule->addDay(new Day('Saturday', 6));
        $schedule->addDay(new Day('Sunday', 6));

        $this->dto = new DTO([
            new Discipline('math'),
            new Discipline('math'),
            new Discipline('math'),
            new Discipline('chem'),
            new Discipline('chem'),
            new Discipline('hist'),
        ], $schedule);
    }

    public function analyse()
    {
        $i = 0;
        while (!$this->stop()) {


            if ($i > 1000) {
                break;
            }
            $i++;
        }
    }

    public function build()
    {
        $disciplines = $this->dto->getDiscipline();
        $schedule = $this->dto->getSchedule();

        foreach ($disciplines as $discipline) {
            /** @var Day $day */
            foreach ($schedule->getDays() as $day) {
                $day->add($discipline);
            }
        }

        $schedule->print();
    }

    public function stop()
    {
        return false;
    }

    public function down()
    {

    }
}