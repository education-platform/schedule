<?php

namespace Schedule;

use Schedule\Console\Colors;

trait ConsoleHelper
{
    use Colors;

    public function text($message)
    {
        $this->echo($message);
    }

    public function info($message)
    {
        $this->echo($message, $this->blue());
    }

    public function success($message)
    {
        $this->echo($message, $this->green());
    }

    public function separation()
    {
        $sep = "<";

        for ($i = 0; $i < 50; $i++) {
            $sep .= "-";
        }

        $this->echo($sep.">", $this->magenta());
    }

    public function echo($message, $color = '')
    {
        if ($color) {
            echo $color;
        } else {
            $this->resetColor();
        }

        echo $message."\n";

        $this->resetColor();
    }

    public function resetColor()
    {
        echo $this->white();
    }
}