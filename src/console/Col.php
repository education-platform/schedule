<?php

namespace Schedule\Console;

class Col
{
    public array $cells = [];
    public int $width = 0;

    public function addCell(Cell $cell)
    {
        $this->cells[] = $cell;

        if ($cell->width > $this->width) {
            $this->width = $cell->width;
        }
    }

    public function get(int $index): Cell
    {
        /** @var Cell $cell */
        $cell = $this->cells[$index] ?? new Cell();

        return $cell->levelingTo($this->width);
    }

    public function count(): int
    {
        return count($this->cells);
    }
}