<?php

namespace Schedule\Console;

use Schedule\ConsoleHelper;
use Schedule\Day;
use Schedule\Discipline;
use Schedule\Schedule;

class ScheduleTable
{
    use ConsoleHelper;

    /**
     * @var Schedule
     */
    protected Schedule $schedule;

    private array $columns = [];
    private array $rows = [];

    private int $rowsHeight = 0;
    private int $tableWidth = 0;

    public function __construct(Schedule $schedule)
    {
        $this->schedule = $schedule;
    }

    public function build(): void
    {
        /** @var Day $day */
        foreach ($this->schedule->getDays() as $day) {

            $col = new Col();

            $col->addCell(new Cell($day->name));

            /** @var Discipline $discipline */
            foreach ($day->disciplines as $discipline) {
                $col->addCell(new Cell($discipline->name));
            }

            $this->pushColumn($col);
        }

        $this->calcTableWidth();
        $this->transpose();
    }

    private function pushColumn(Col $col): void
    {
        $this->columns[] = $col;

        if ($this->rowsHeight < $col->count()) {
            $this->rowsHeight = $col->count();
        }
    }

    private function pushRow(Row $row): void
    {
        $this->rows[] = $row;
    }

    private function calcTableWidth(): void
    {
        /** @var int $colsWidth */
        $colsWidth = array_reduce($this->columns, function ($c, $item) {
            $c += $item->width;

            return $c + 2;
        });

        $this->tableWidth = $colsWidth + count($this->columns) + 1;
    }

    private function transpose(): void
    {
        for ($j = 0; $j < $this->rowsHeight; $j++) {
            $row = new Row();
            for ($i = 0; $i < count($this->columns); $i++) {
                if ($j == 0) {
                    $row->titled();
                }

                $row->addCell($this->columns[$i]->get($j));
            }
            $this->pushRow($row);
        }
    }

    public function print(): void
    {
        $this->build();

        $this->line($this->tableWidth);

        /** @var Row $row */
        foreach ($this->rows as $row) {
            $row->print();

            if ($row->isTitled()) {
                $this->line($this->tableWidth);
            }
        }

        $this->line($this->tableWidth);
    }

    private function line(int $length): void
    {
        $line = '';

        for ($i = 0; $i < $length; $i++) {
            $line .= '-';
        }

        $this->echo($line, $this->magenta());
    }
}