<?php

namespace Schedule\Console;

use Schedule\ConsoleHelper;

class Row
{
    use ConsoleHelper;

    private array $cells = [];
    private bool $titled = false;

    public function addCell(Cell $cell): void
    {
        $this->cells[] = $cell;
    }

    public function print(): void
    {
        $context = '';

        /** @var Cell $cell */
        foreach ($this->cells as $cell) {
            $context .= ' '.$cell->getContext().' '.$this->vl();
        }

        $this->echo($this->vl().trim($context, $this->vl()).$this->vl());
    }

    private function vl(): string
    {
        return $this->magenta()."|".$this->white();
    }

    public function isTitled(): bool
    {
        return $this->titled;
    }

    public function titled(): void
    {
        $this->titled = true;
    }
}