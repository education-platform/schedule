<?php

namespace Schedule\Console;

class Cell
{
    use Colors;

    private string $context;
    private string $color;

    public int $width;

    public function __construct(string $context = '', string $color = null)
    {
        $this->context = $context;
        $this->color = $color ?? $this->white();
        $this->width = strlen($context);
    }

    public function getContext(): string
    {
        return $this->color.$this->context.$this->white();
    }

    public function levelingTo(int $width): Cell
    {
        $emptySpaceLen = $width - strlen($this->context);

        if ($emptySpaceLen > 0) {
            for ($i = 0; $i < $emptySpaceLen; $i++) {
                $this->context .= " ";
            }
        }

        return $this;
    }
}