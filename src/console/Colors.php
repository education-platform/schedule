<?php

namespace Schedule\Console;

trait Colors
{
    public function green()
    {
        return "\e[32m";
    }

    public function red()
    {
        return "\e[31m";
    }

    public function yellow()
    {
        return "\e[33m";
    }

    public function magenta()
    {
        return "\e[95m";
    }

    public function white()
    {
        return "\e[39m";
    }

    public function blue()
    {
        return "\e[34m";
    }
}