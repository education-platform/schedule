<?php

namespace Schedule;

class Day
{
    public $crowded = false;
    public $name;
    public $maxDiscipline;
    public $disciplines = [];

    public function __construct($name, $maxDiscipline)
    {
        $this->name = $name;
        $this->maxDiscipline = $maxDiscipline;
    }

    public function add($discipline) {
        $this->disciplines[] = $discipline;
    }

    public function crowded()
    {
        return $this->crowded || $this->maxDiscipline == count($this->disciplines);
    }
}