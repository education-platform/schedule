<?php

use Schedule\Main;

require __DIR__ . '/../vendor/autoload.php';

$main = new Main();
$main->run();